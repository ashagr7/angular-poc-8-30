import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-sub-section',
  templateUrl: './sub-section.component.html',
  styleUrls: ['./sub-section.component.scss']
})
export class SubSectionComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Input() subSection: any

}
