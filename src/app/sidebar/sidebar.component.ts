import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Section } from "../section/section";

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }


  @Input() sections: any
  @Input() selectedSection: any
  @Output("selectSection") selectSection: EventEmitter<Section> = new EventEmitter();

}
