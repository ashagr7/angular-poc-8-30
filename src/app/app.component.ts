import { Component } from '@angular/core';
import { Section } from './section/section'
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})

export class AppComponent {
  title = 'Angular-POC';
  sections = [
    {
      id: "Section1",
      title: 'Section 1', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }, { title: 'Label 3' }, { title: 'Label 4' }, { title: 'Label 5' }],
      subSections: [
        { id: 'SubSection1', title: 'Sub-Section 1', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }, { title: 'Label 3' }, { title: 'Label 4' }, { title: 'Label 5' }, { title: 'Label 6' }] },
        { id: 'SubSection2', title: 'Sub-Section 2', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }] }

      ]
    },
    {
      id: "Section2",
      title: 'Section 2', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }, { title: 'Label 3' }],
      subSections: [{ id: 'SubSection1', title: 'Sub-Section 1', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }] }]
    },
    {
      id: "Section3",
      title: 'Section 3', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }, { title: 'Label 3' }, { title: 'Label 4' }, { title: 'Label 5' }],
      subSections: [{ id: 'SubSection1', title: 'Sub-Section 1', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }] },]
    }
  ];

  selectedSection: Section = this.sections[0];

  selectSection(data) {
    this.selectedSection = data;
  }

  addSection() {
    const sectionId = `Section${this.sections.length + 1}`;
    const sectionName = `Section ${this.sections.length + 1}`;
    const newSection = {
      id: sectionId,
      title: sectionName,
      formFields: [{ title: 'Label 1' }, { title: 'Label 2' }, { title: 'Label 3' }],
      subSections: [{ id: 'SubSection1', title: 'Sub-Section 1', formFields: [{ title: 'Label 1' }, { title: 'Label 2' }] }]
    }
    this.sections.push(newSection);
    this.selectedSection = newSection;
  }

  addSubSection() {
    const subSectionId = `SubSection${this.selectedSection.subSections.length + 1}`;
    const subSectionName = `SubSection ${this.selectedSection.subSections.length + 1}`;
    const newSubsection = {
      id: subSectionId,
      title: subSectionName,
      formFields: [{ title: 'Label 1' }],
    }
    this.selectedSection.subSections.push(newSubsection);
  }

}
