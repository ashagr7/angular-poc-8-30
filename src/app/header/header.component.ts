import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { Section } from "../section/section";

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

  @Output("addSection") addSection: EventEmitter<Section> = new EventEmitter();
  @Output("addSubSection") addSubSection: EventEmitter<any> = new EventEmitter();
}
