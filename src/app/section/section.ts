export interface Section {
    id: string,
    title: string,
    formFields: Array<{}>,
    subSections: Array<{}>
}